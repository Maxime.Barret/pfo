import pyb
 

bouton_vert = pyb.Pin("X1")
bouton_rouge = pyb.Pin("X2")
led = pyb.Pin("X3")
 
bouton_vert.init(pyb.Pin.IN, pyb.Pin.PULL_UP)
bouton_rouge.init(pyb.Pin.IN, pyb.Pin.PULL_UP)
led.init(pyb.Pin.OUT_PP)

def clignote(i):
    if led.value() == 0:
        led.high()
    else:
        led.low()

timer = pyb.Timer(4)
timer.callback(None)
timer.deinit()

def int_vert(i):
    if led.value() == 1:
        led.low()
    timer.init(freq = 1)
    timer.callback(clignote)   


def int_rouge(i):
    timer.deinit()
    led.high()

irq_vert = pyb.ExtInt(bouton_vert, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, None)
irq_rouge = pyb.ExtInt(bouton_rouge, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, None)
irq_vert = pyb.ExtInt(bouton_vert, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, int_vert)
irq_rouge = pyb.ExtInt(bouton_rouge, pyb.ExtInt.IRQ_FALLING, pyb.Pin.PULL_UP, int_rouge)
