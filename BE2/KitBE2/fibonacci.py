def fibonacci(n):
    if n <= 1:
        return n
    else:
        fibo = 1
        fiboPrev = 1
        i = 2
        while i < n :
            temp = fibo
            fibo += fiboPrev
            fiboPrev = temp
            i += 1
        return fibo
        
print(fibonacci(6))