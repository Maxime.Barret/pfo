# '''
#  Schema matrice

#    &&&&&&&&&&&&&&&&&&&&&&&&&
#    &&&&&&&&&&&&&&&&&&&&&&&&&   <-- MAX7219
#     __ __ __ __ __ __ __ __ 
#    |                       | 2**7 : x = 7
#    |                       | 2**6 : x = 6
#    |                       | 2**5 : x = 5 
#    |                       | 2**4 : x = 4    bitemap[y] (octets)
#    |                       | 2**3 : x = 3
#    |                       | 2**2 : x = 2
#    |                       | 2**1 : x = 1                  
#    |__ __ __ __ __ __ __ __| 2**0 : x = 0
#     08 07 06 05 04 03 02 01
#              < y >  
#            (registres)                 
# '''

def testPixels():
    bitmap = bytearray(8)
    matrixOn(1)
    matrixTest(0)
    matrixDecode(0)
    matrixDigits(7)
    clearDisplay(bitmap)
    # Trace un O
    setPixel(1,2,1, bitmap); setPixel(2,2,1, bitmap); setPixel(3,2,1, bitmap)
    setPixel(1,3,1, bitmap);                          setPixel(3,3,1, bitmap)
    setPixel(1,4,1, bitmap);                          setPixel(3,4,1, bitmap)
    setPixel(1,5,1, bitmap); setPixel(2,5,1, bitmap); setPixel(3,5,1, bitmap)
    # Trace un K
    setPixel(5,2,1, bitmap);                          setPixel(7,2,1, bitmap)
    setPixel(5,3,1, bitmap); setPixel(6,3,1, bitmap)
    setPixel(5,4,1, bitmap); setPixel(6,4,1, bitmap)
    setPixel(5,5,1, bitmap);                          setPixel(7,5,1, bitmap)
    updateDisplay(bitmap)
 
def testPixels2():
    bitmap = bytearray(8)
    matrixOn(True)
    matrixTest(False)
    matrixDecode(False)
    matrixDigits(7)
    clearDisplay(bitmap)
    x = 0
    y = 0
    while True:
        setPixel(x, y, True, bitmap)
        updateDisplay(bitmap)
        setPixel(x, y, False, bitmap)
        x = (x + 1) % 8
        if x == 0:
            y = (y + 1) % 8
        pyb.delay(100)

def displayPict(pict):
    bitmap = bytearray(8)
    matrixOn(True)
    matrixTest(False)
    matrixDecode(False)
    matrixDigits(7)
    clearDisplay(bitmap)

    for i in range(0, 8):
        line = pict[i]
        for j in range(0, 8):
            if line[j] == " ":
                setPixel(i,j,False,bitmap)
            else:
                setPixel(i,j,True,bitmap)
    updateDisplay(bitmap)
    return bitmap

smiley = (
    "  ****  ",
    " *    * ",
    "* *  * *",
    "*      *",
    "* *  * *",
    "*  **  *",
    " *    * ",
    "  ****  "
)
frowney = (
    "  ****  ",
    " *    * ",
    "* *  * *",
    "*      *",
    "*  **  *",
    "* *  * *",
    " *    * ",
    "  ****  "
)
