#
# Practical session #2: Drive a 8x8 LED matrix using a MAX7219
#
# frederic.boulanger@centralesupelec.fr
# 2015-08-20
# copyright CentraleSupelec
#
import pyb
 
data = "X3";    # patte connectée à l'entrée série du MAX7219 (DIN)
load = "X2";    # patte de chargement des données (CS)
clk  = "X1";    # patte donnant l'horloge de la liaison série (CLK)
 
# Initialisation des pattes en sortie en mode push-pull
dataPin = pyb.Pin(data, pyb.Pin.OUT_PP)
loadPin = pyb.Pin(load, pyb.Pin.OUT_PP)
clkPin  = pyb.Pin(clk, pyb.Pin.OUT_PP)
 
# Mise à zéro des pattes
dataPin.low()
loadPin.low()
clkPin.low()
 
# Transmet un octet bit par bit vers le MAX7219, bit de poids fort en premier
def serialShiftByte(data):
    # Mise à zéro du signal d'horloge pour pouvoir faire un front montant plus tard
    clkPin.low()
    # Décalage des 8 bits de données
    for i in range(8):
        # on décale la donnée de i bits vers la gauche et on teste le bit de poids fort
        value = ((data << i) & 0B10000000) != 0
        dataPin.value(value)  # on met la patte DIN à cette valeur
        clkPin.high()         # puis on crée une impulsion sur CLK
        clkPin.low()          # pour transmettre ce bit
 
# Écriture d'une donnée dans un registre du MAX7219.
def serialWrite(address, data):
    # Mise à zéro du signal CS pour pouvoir créer un front montant plus tard
    loadPin.low()
    # On envoie l'adresse en premier
    serialShiftByte(address)
    # puis la donnée
    serialShiftByte(data)
    # et on crée une impulsion sur la ligne CS pour charger la donnée dans le registre
    loadPin.high()
    loadPin.low()



def matrixOn(on):
    if on:
        serialWrite(0x0C, 0x01) # Start
    else:
        serialWrite(0x0C, 0x0) # Shutdown


def matrixTest(test):
    if test:
        serialWrite(0x0F, 0x01) # Display test on
    else:
        serialWrite(0x0F, 0x00) # Display test off


def matrixIntensity(percent):
    serialWrite(0x0A, percent * 15 // 100 )


def matrixDecode(decode):
    if decode:
        serialWrite(0x09, 0xFF) # Decode = 1
    else:
        serialWrite(0x09, 0x00) # Decode = 0


def matrixDigits(num):
    if num < 8:
        serialWrite(0x0B, num) # Scan limit (max 0x07)
    else:
        print("ERR matrixDigits -- num trop grand :: ", num)


def matrixLine(num, value):
    if num < 9 and num > 0:
        serialWrite(num, value)
    else:
        print("ERR matrixLine : num invalide :: ", num)


def clear():
    for i in range(1, 9):
        matrixLine(i, 0)



def updateDisplay(bitmap):
    for i in range(0,8):
        matrixLine(i+1, bitmap[i])


def clearDisplay(bitmap):
    for i in range(0,8):
        bitmap[i] = 0x00
    updateDisplay(bitmap)

def setPixel(x,y,on,bitmap):
    if on:
        bitmap[y] = bitmap[y] | 2**x
    else:
        bitmap[y] = bitmap[y] & ~(2**x)

def getPixel(x,y,bitmap):
    if (bitmap[y] & 2**x) != 0:
        return True
    else:
        return False

