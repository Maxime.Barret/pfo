import random


def randomBitmap():
    bitmap = bytearray(8)
    for i in range(0,8):
        bitmap[i] = random.randint(0,255)
    return bitmap

def countNeighbours(x,y,bitmap):
    '''
      4 3 2      
      5 # 1
      6 7 8
    '''
    count = 0
    if getPixel((x+1)%8, y, bitmap):            # 1
        count += 1
    if getPixel((x+1)%8, (y+1)%8, bitmap):      # 2
        count += 1
    if getPixel(x, (y+1)%8, bitmap):            # 3
        count += 1
    if getPixel((x-1)%8, (y+1)%8, bitmap):      # 4
        count += 1
    if getPixel((x-1)%8, y, bitmap):            # 5
        count += 1
    if getPixel((x-1)%8, (y-1)%8, bitmap):      # 6
        count += 1
    if getPixel(x, (y-1)%8, bitmap):            # 7
        count += 1
    if getPixel((x+1)%8, (y-1)%8, bitmap):      # 8
        count += 1
    return count

def lifeStep(bitmap):
    bitmap_copie = bytearray(8)
    for i in range(8):
        bitmap_copie[i] = bitmap[i]

    for y in range(0,8):
        for x in range(0,8):
            c = countNeighbours(x,y,bitmap)
            if c == 3:
                setPixel(x,y,True,bitmap_copie)
            elif c == 2:
                continue
            else:
                setPixel(x,y,False,bitmap_copie)
    
    for i in range(8):
        bitmap[i] = bitmap_copie[i]

    updateDisplay(bitmap)

def gameOfLife():
    matrixOn(True)
    matrixTest(False)
    matrixDecode(False)
    matrixDigits(7)
    bitmap = randomBitmap()
    updateDisplay(bitmap)
    pyb.delay(200)
    while True:
        lifeStep(bitmap)
        pyb.delay(200)


