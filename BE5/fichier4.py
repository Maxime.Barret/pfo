stableBlock = (
  "        ",
  "        ",
  "        ",
  "   **   ",
  "   **   ",
  "        ",
  "        ",
  "        "
)
def testBlock():
    bitmap = displayPict(stableBlock)
    pyb.delay(500)
    lifeStep(bitmap)
 
stableTube = (
  "        ",
  "        ",
  "   *    ",
  "  * *   ",
  "   *    ",
  "        ",
  "        ",
  "        "
)
def testTube():
    bitmap = displayPict(stableTube)
    pyb.delay(500)
    lifeStep(bitmap)
 
# Figures oscillantes
oscBlinker = (
  "        ",
  "        ",
  "        ",
  "  ***   ",
  "        ",
  "        ",
  "        ",
  "        "
)
def testBlinker():
    bitmap = displayPict(oscBlinker)
    for i in range(2):
        pyb.delay(500)
        lifeStep(bitmap)
 
# Vaisseaux
shipGlider = (
  "        ",
  "        ",
  "        ",
  "        ",
  "        ",
  "***     ",
  "  *     ",
  " *      "
)
def testGlider():
    bitmap = displayPict(shipGlider)
    for i in range(32):
        pyb.delay(500)
        lifeStep(bitmap)
 
shipLWSS = (
  "        ",
  "        ",
  "*  *    ",
  "    *   ",
  "*   *   ",
  " ****   ",
  "        ",
  "        "
)

def testLWSS():
    bitmap = displayPict(shipLWSS)
    for i in range(16):
        pyb.delay(500)
        lifeStep(bitmap)

