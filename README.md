**Livrable de TP de Principe de Fonctionnement des Ordinateurs**

**_Abstract_**

In this repository, you will find all the projects I did during my computer science lessons. This course has introduced me to multiple subjects like :
- Implementating a Von Neumann model using _Logisim_ and simple logic                            (BE2/BE3)
- Implementating a _Assembly_ language in order to give instructions to our model processor      (BE2/BE3)
- Writing more complexe functions with our assembly language                                     (BE3) 

Moreover, besides these hardware projects, I did some programming for MicroPython (an Arduino board but computing Python scripts) in the BE4 and BE5 directories. The BE5 directory regroups codes for a Game Of Life on a MicroPython board using a 8x8 led matrix.


**Résumé**

Dans ce répertoire, se trouvent tous les projets que j'ai réalisé en cours de Principe de Fonctionnement des Ordinateurs (PFO). Ce cours m'a introduit à divers notion comme :
- Implémenter un modèle de Von Neumann en utilisant le logiciel _Logisim_ et de la logic simple     (BE2/BE3)
- Implémenter un langage type _Assembleur_ pour pouvoir donner des instructions à notre processeur  (BE2/BE3)
- Ecrire des fonctions complexes dans notre langage                                                 (BE3)

De plus, à part ces projets orientés harware, j'ai codé quelques fonctionnalités pour une carte MicroPython (une carte Arduino mais compilant du code Python) dans les répertoires BE4 et BE5. Plus particulièrement, BE5 regroupe des codes utilisés pour un Jeu de la Vie sur une matrice de led 8x8.

